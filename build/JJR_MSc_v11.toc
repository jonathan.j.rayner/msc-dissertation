\babel@toc {english}{}
\contentsline {chapter}{Abstract}{iii}{section*.1}% 
\contentsline {chapter}{Acknowledgements}{v}{section*.2}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Background: The Color Glass Condensate and the JIMWLK equation}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Purpose of this dissertation}{3}{section.1.2}% 
\contentsline {chapter}{\numberline {2}A novel parameterisation of JIMWLK evolution}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}The Problem: JIMWLK generates an infinite hierarchy of coupled differential equations}{6}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Dipole correlator evolution depends on the 3-point function}{9}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}The evolution of higher correlators is naturally organised as a matrix equation}{11}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2.2}A brief summary: some existing approximate solutions to the JIMWLK equation and their limitations}{13}{section.2.2}% 
\contentsline {section}{\numberline {2.3}A novel parameterisation gives us a finite $N_c$ truncation that respects coincidence limits}{15}{section.2.3}% 
\contentsline {chapter}{\numberline {3}New color singlet space calculations}{23}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Color singlet space}{24}{section.3.1}% 
\contentsline {section}{\numberline {3.2}The trace basis}{25}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Hermitian Young projection operator basis}{26}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Outline of the construction}{27}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Hermitian Young projectors applied to the trace basis for $k = 2, 3$}{31}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}New calculation: Hermitian Young projection operator basis, $k = 4$}{33}{subsection.3.3.3}% 
\contentsline {section}{\numberline {3.4}Summary of \Cref *{chapter_color}}{37}{section.3.4}% 
\contentsline {chapter}{\numberline {4}Tricks from representation theory teach us more about the Hermitian Young projection operator basis}{39}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Characters give us the multiplicity of irreducible representations}{41}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Modules, induced representations, and Frobenius reciprocity}{45}{section.4.2}% 
\contentsline {section}{\numberline {4.3}The construction}{51}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Examples for $k = 2,3,4$}{57}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}$k=2$}{57}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}$k=3$}{58}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}$k=4$}{59}{subsection.4.4.3}% 
\contentsline {section}{\numberline {4.5}Characters tell us which adjoint color structures are real or purely imaginary}{62}{section.4.5}% 
\contentsline {subsection}{\numberline {4.5.1}Examples: real and purely imaginary irreducible modules for $k=2,3,4$}{66}{subsection.4.5.1}% 
\contentsline {section}{\numberline {4.6}Summary of \Cref *{chapter_basis_theory}}{69}{section.4.6}% 
\contentsline {chapter}{\numberline {5}A digression: bounding simple Wilson line operators}{71}{chapter.5}% 
\contentsline {section}{\numberline {5.1}The image of the trace of $SU(n)$ matrices is the region bounded by the $n$-cusp hypocycloid}{72}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Bounds on $(q\bar {q})^2$}{78}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Concluding remarks on bounding these operators}{82}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Conclusions and outlook}{83}{chapter.6}% 
\contentsline {section}{\numberline {6.1}New results established in this dissertation}{84}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Open questions for future research}{85}{section.6.2}% 
\contentsline {chapter}{\numberline {A}Birdtracks for our purposes}{87}{appendix.A}% 
\contentsline {chapter}{\numberline {B}Proofs and calculations encountered in constructing the adjoint color structures}{89}{appendix.B}% 
\contentsline {section}{\numberline {B.1}Intermediate steps in calculating the adjoint color structures, $k=4$}{89}{section.B.1}% 
\contentsline {section}{\numberline {B.2}Deriving Frobenius reciprocity for characters}{94}{section.B.2}% 
\contentsline {section}{\numberline {B.3}A Proof that $r_{\mu } \in N_{S_k}(C_{S_k}(\sigma _{\mu }))$}{97}{section.B.3}% 
\contentsline {chapter}{\numberline {C}Proofs necessary for bounding the images of simple Wilson line correlators}{99}{appendix.C}% 
\contentsline {section}{\numberline {C.1}\Cref {hypocycloids_f_smooth}}{99}{section.C.1}% 
\contentsline {section}{\numberline {C.2}\Cref {criticalvalueboundarylemma}}{100}{section.C.2}% 
\contentsline {section}{\numberline {C.3}\Cref {lemmarank0criticalvalues}}{100}{section.C.3}% 
\contentsline {section}{\numberline {C.4}\Cref {hypocycloid_critical_points_theorem_1}}{101}{section.C.4}% 
\contentsline {section}{\numberline {C.5}\Cref {hypocycloid_o12_unit_circle}}{106}{section.C.5}% 
\contentsline {section}{\numberline {C.6}\Cref {hypocycloid_o22_unit_circle}}{107}{section.C.6}% 
\contentsline {chapter}{Bibliography}{111}{appendix*.24}% 
